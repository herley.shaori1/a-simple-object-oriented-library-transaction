package library;

import item.Book;
import person.Person;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Library {
    private Map<String,List> reservations = new HashMap<>();
    public void createReservation(String reservationID, Person person, Book book){
        // Add condition so that Lecturer cannot reserve a fiction book.
        if(person.getStatus().equals("lecturer") && book.getID().contains("fic")){
            System.out.println("Book reservation failed because a Lecturer cannot reserve a fiction Book.");
        }else{
            List<Object> reservationDetail = new ArrayList();
            reservationDetail.add(person);
            reservationDetail.add(book);
            this.reservations.put(reservationID,reservationDetail);
            System.out.println("Reservation process succeeded.");
        }
    }

    public void getReservation(String reservationID){
        List list = this.reservations.get(reservationID);
        if(list == null){
            System.out.println("ReservationID not found.");
        }else{
            System.out.println("ReservationID: "+reservationID);
            System.out.println("Book:");
            System.out.println(list.get(1));
            System.out.println("Reserved by:");
            System.out.println(list.get(0));
        }
    }

    public void deleteReservation(String reservationID){
        this.reservations.remove(reservationID);
        System.out.println("ReservationID successfully removed");
    }
}
