import item.Book;
import library.Library;
import person.Lecturer;
import person.Student;

public class Main {
    public static void main(String[] args) {
        // Student initializations.
        Student stuOne = new Student("Budi","budi@domain.id");
        Lecturer lecOne = new Lecturer("Ani","ani@domain.id");

        // Book initializations.
        Book mathBook = new Book("math123","Introduction to Algebra.");
        Book fictionBook = new Book("fic123","Harry Potter");

        // Make a reservations.
        Library library = new Library();
        library.createReservation("rsvp0",lecOne,mathBook);
        // This reservation is expected to be failed because a Lecturer cannot reserve a fiction book.
        library.createReservation("rsvp1",lecOne,fictionBook);

        // Get a reservation.
        library.getReservation("rsvp0");
//        library.getReservation("rsvp1");

        // Delete a reservation.
        library.deleteReservation("rsvp0");
        library.getReservation("rsvp0");
    }
}
