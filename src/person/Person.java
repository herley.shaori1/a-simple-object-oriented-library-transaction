package person;

public abstract class Person {
    protected String name;
    protected String email;

    protected Person(String name,String email){
        this.name = name;
        this.email = email;
    }

    public String getName(){
        return this.name;
    }

    public String getEmail(){
        return this.email;
    }

    public abstract String getStatus();
}
