package person;

public class Lecturer extends Person{
    public Lecturer(String name,String email){
        super(name,email);
    }
    @Override
    public String getStatus() {
        return "lecturer";
    }

    @Override
    public String toString(){
        return "Name: "+this.name+"\n"+"Email: "+this.email;
    }
}
