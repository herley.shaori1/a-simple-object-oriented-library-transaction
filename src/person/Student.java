package person;

public class Student extends Person{
    public Student(String name,String email){
        super(name,email);
    }

    @Override
    public String getStatus(){
        return "student";
    }
}
