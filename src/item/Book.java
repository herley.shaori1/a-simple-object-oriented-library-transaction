package item;

public class Book {
    private String id;
    private String name;

    public Book(String id,String name){
        this.id=id;
        this.name=name;
    }

    public String getID(){
        return this.id;
    }

    public String getName(){
        return this.name;
    }

    @Override
    public String toString(){
        return "ID: "+this.id+"\n"+"Name: "+this.name;
    }
}
